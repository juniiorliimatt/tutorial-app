import { TutorialService } from './../../services/tutorials-service/tutorial.service';
import { Component, OnInit } from '@angular/core';
import { Tutorial } from 'src/app/models/tutorial';

@Component({
  selector: 'app-tutorials-list',
  templateUrl: './tutorials-list.component.html',
  styleUrls: ['./tutorials-list.component.scss'],
})
export class TutorialsListComponent implements OnInit {
  tutorials?: Tutorial[];
  currentTutorial: Tutorial = {};
  currentIndex = -1;
  title = '';

  constructor(private service: TutorialService) {}

  ngOnInit(): void {
    this.atualizarLista();
  }

  async recuperarTutoriais(): Promise<void> {
    this.service.getAllTutorials().subscribe(
      (data) => {
        this.tutorials = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  atualizarLista(): void {
    this.recuperarTutoriais();
    this.currentTutorial = {};
    this.currentIndex = -1;
  }

  definirTutorialAtivo(tutorial: Tutorial, index: number): void {
    this.currentTutorial = tutorial;
    this.currentIndex = index;
  }

  deleteAllTutorials(): void {
    this.service.deleteAllTutorials().subscribe(
      (response) => {
        console.log(response);
        this.atualizarLista();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  pesquisarPorTitulo(): void {
    this.currentTutorial = {};
    this.currentIndex = -1;

    this.service.getByTitle(this.title).subscribe(
      (data) => {
        this.tutorials = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
