import { TutorialService } from './../../services/tutorials-service/tutorial.service';
import { Tutorial } from './../../models/tutorial';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-tutorial-details',
  templateUrl: './tutorial-details.component.html',
  styleUrls: ['./tutorial-details.component.scss'],
})
export class TutorialDetailsComponent implements OnInit {
  currentTutorial: Tutorial = {
    title: '',
    description: '',
    published: false,
  };

  message = '';

  constructor(
    private service: TutorialService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.message = '';
    this.getTutorial(this.route.snapshot.params.id);
  }

  getTutorial(id: number): void {
    this.service.get(id).subscribe(
      (data) => {
        this.currentTutorial = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  updatePublished(status: boolean): void {
    const data = {
      title: this.currentTutorial.title,
      description: this.currentTutorial.description,
      published: status,
    };

    this.message = '';

    this.service.updateTutorial(this.currentTutorial.id, data).subscribe(
      (response: any) => {
        this.currentTutorial.published = status;
        console.log(response);
        this.message = response.message
          ? response.message
          : 'The status was updated';
      },
      (error) => {
        console.log(error);
      }
    );
  }

  updateTutorial(): void {
    this.message = '';
    this.service
      .updateTutorial(this.currentTutorial.id, this.currentTutorial)
      .subscribe(
        (response: any) => {
          console.log(response);
          this.message = response.message
            ? response.message
            : 'This tutorial was updated successfully!';
        },
        (error) => {
          console.log(error);
        }
      );
  }

  deleteTutorial(): void {
    this.service.deleteTutorial(this.currentTutorial.id).subscribe(
      (response: any) => {
        console.log(response);
        this.router.navigate(['/tutorials']);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
