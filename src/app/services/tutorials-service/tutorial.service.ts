import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Tutorial } from '../../models/tutorial';

const baseUrl = 'https://api-oojuniin.herokuapp.com/api/tutorials';

@Injectable({
  providedIn: 'root',
})
export class TutorialService {
  constructor(private http: HttpClient) {}

  get(id: number): Observable<Tutorial> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  getAllTutorials(): Observable<Tutorial[]> {
    return this.http.get<Tutorial[]>(baseUrl);
  }

  getByTitle(title: string): Observable<Tutorial[]> {
    return this.http.get<Tutorial[]>(`${baseUrl}?title=${title}`);
  }

  createTutorial(data: Tutorial): Observable<Tutorial> {
    return this.http.post(baseUrl, data);
  }

  updateTutorial(id: any, data: Tutorial): Observable<Tutorial> {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  deleteTutorial(id: any): Observable<Tutorial> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAllTutorials(): Observable<Tutorial> {
    return this.http.delete(`${baseUrl}`);
  }
}
